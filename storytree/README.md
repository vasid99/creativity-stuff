# Storytree
A Python API to create branching storylines, inspired from (Twine)[https://twinery.org/].
I'm not really sure right now why I'm doing this, but maybe time will tell.
**Usage**: Coming soon/pull requests welcome ;) (it's still raw, so not that usable anyways). `story1.py` is provided as an example.

## To-do
1. Implement failsafes
2. Textual story viewing and non-additive editing (i.e. not just adding stuff, but also changing and/or removing existing stuff)

## Ideas for the Future
1. GUI (table-and-graph interface)
2. Cross-platform with JS and develop web app (since the exporting is done in JSON, this seems to be achievable)
