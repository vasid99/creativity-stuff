from collections import defaultdict
import json
import logging as _logging

# configure logger
_logging.basicConfig(filename="storytree.log",level=_logging.INFO,filemode="w")

class storyV1:
  def __init__(self,loadfile=None):
    if loadfile==None:
      print("Creating new blank story")
      self.choices = {}
      self.contents = {}
      self.cgraph = defaultdict(list)
      self.revcgraph = defaultdict(list)
      self.startChoice = ""
    else:
      self.loadStory(loadfile)
  
  def loadStory(self,loadfile):
    print("Attempting to load story from %s"%loadfile)
    
    try:
      f=open(loadfile,"r")
    except:
      print("Load unsuccessful. Please check if the load file exists.")
      exit()
    
    try:
      loadObj = json.load(f)
    except:
      print("Load unsuccessful. Please check if the load file follows the JSON file format.")
      f.close()
      exit()
    f.close()
    
    try:
      self.choices = loadObj["choices"]
      self.contents = loadObj["contents"]
      self.cgraph = loadObj["cgraph"]
      self.revcgraph = loadObj["revcgraph"]
      self.startChoice = loadObj["startChoice"]
    except:
      print("Load unsuccessful. The file does not follow the Storytree format. \
      Please check the wiki page on this (PUT LINK HERE) to resolve the issue.")
      exit()
      
    print("Load complete")
  
  def saveStory(self,savefile):
    print("Saving current progress to %s"%savefile.name)
    saveObj = {"choices":self.choices, 
    "contents":self.contents, 
    "cgraph":self.cgraph, 
    "revcgraph":self.revcgraph, 
    "startChoice":self.startChoice}
    json.dump(saveObj,savefile,indent=4)
    print("Save complete")
  
  def addChoice(self,cid,choice,content):
    wr = True
    if cid in self.choices.keys():
      print("Warning - ID %s already exists. Overwrite(Oo) or skip(Ss)?"%cid)
      while True:
        x=input()
        if x in ['O','o']:
          print("Overwriting choice %s"%cid)
          break
        elif x in ['S','s']:
          print("Skipping choice %s"%cid)
          wr=False
          break
        else:
          print("Invalid option %s. Please enter one of OoSs"%x)
    else:
      print("Adding choice %s"%cid)
    if wr:
      self.choices[cid] = choice
      self.contents[cid] = content
  
  def connectChoices(self,parentID,childID):
    if parentID in self.choices.keys():
      if childID in self.choices.keys():
        print("Connecting choices")
        self.cgraph[parentID].append(childID)
        self.revcgraph[childID].append(parentID)
      else:
        print("Destination choice ID %s does not exist. Exiting"%childID)
        exit()
    else:
      print("Source choice ID %s does not exist. Exiting"%parentID)
      exit()
  def setStartChoice(self,cid):
    if cid in self.choices.keys():
      print("Setting start choice")
      self.startChoice = cid
    else:
      print("Choice ID %s does not exist. Exiting"%cid)
      exit()
  
  def editChoice(self,cid,choice=None,content=None):
    if cid in self.choices.keys():
      if not choice==None:
        print("Replacing choice text of %s"%cid)
        self.choices[cid] = choice
      if not content==None:
        print("Replacing choice contents of %s"%cid)
        self.contents[cid] = content
    else:
      print("Choice ID %s does not exist. Exiting"%cid)
      exit()
  
  def printChoice(self,cid):
    if cid in self.choices.keys():
      print("Choice ID:\n%s\nChoice text:\n%s\nChoice contents:\n%s"%(cid,self.choices[cid],self.contents[cid]))
    else:
      print("Choice ID %s does not exist. Exiting"%cid)
      exit()
  
  def deleteChoice(self,cid,deleteFurtherChoices=True):
    # delete all child nodes too?

class story(storyV1):
  pass
