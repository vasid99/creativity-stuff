from collections import defaultdict
import json

class storyV1:
  def __init__(self,loadFile=None):
    print("initing story")
    if loadFile==None:
      self.choices = {}
      self.contents = {}
      self.cgraph = defaultdict(list)
      self.revcgraph = defaultdict(list)
      self.startChoice = None
    else:
      self.loadStory(loadFile)
  
  def loadStory(self,loadFile):
    print("attempting story load from %s"%loadFile.name)
    loadObj = json.load(loadFile)
    self.choices = loadObj["choices"]
    self.contents = loadObj["contents"]
    self.cgraph = loadObj["cgraph"]
    self.revcgraph = loadObj["revcgraph"]
    self.startChoice = loadObj["startChoice"]
    print("load complete")
  
  def saveStory(self,saveFile):
    print("saving current progress to %s"%saveFile.name)
    saveObj = {"choices":self.choices, 
    "contents":self.contents, 
    "cgraph":self.cgraph, 
    "revcgraph":self.revcgraph, 
    "startChoice":self.startChoice}
    json.dump(saveObj,saveFile,indent=4)
    print("save complete")
  
  def addChoice(self,cid,cstr,ccontent):
    print("adding choice")
    # add overwrite failsafes (assert statements)
    self.choices[cid] = cstr
    self.contents[cid] = ccontent
  
  def connectChoices(self,parentID,childID):
    print("connecting choices")
    # add ID existence failsafes
    self.cgraph[parentID].append(childID)
    self.revcgraph[childID].append(parentID)
  
  def setStartChoice(self,cid):
    print("setting start choice")
    # add ID existence failsafes
    self.startChoice = cid
    

class story(storyV1):
  pass
