from collections import defaultdict
import json
import logging as _storylogging

class storyutils:
  def __init__(self):
    # configure logger
    _storylogging.basicConfig(filename="storytree.log",level=_storylogging.INFO,filemode="w")
    self.printopt=lambda s:print("%s"%('' if s==None else (s+'\n')),end="")
  
  def catchAssert(self,assertion,falseprint,trueprint=None):
    # add multi-assert variant
    try:
      assert assertion
      if not trueprint==None:
        print(trueprint)
    except AssertionError:
      print(falseprint)
      exit()
  
  def printinfo(self,info):
    # add info logging
    print('INFO: '+info)
  
  def inputOptLoop(self,opts,**kwargs):  
    # take logging level in kwargs
    
    optcons=""
    for opt in opts:
      optcons += "".join(opt)
    
    if "topmsg" in kwargs:
      self.printopt(kwargs["topmsg"])
    else:
      self.printopt("Choose an option (%s)"%optcons)
    
    while True:
      x=input()
      for i in range(len(opts)):
        if x in opts[i]:
          if "chosenmsg" in kwargs:
            self.printopt(kwargs["chosenmsg"][i])
          return opts[i][0]
      if "invalidmsg" in kwargs:
        self.printopt(kwargs["invalidmsg"])
      else:
        self.printopt("Invalid option %s. Please enter one of %s"%(x,optcons))

class storyV1:
  def __init__(self,loadfile=None):
    self.h = storyutils()
    if loadfile==None:
      self.createBlankStory()
    else:
      self.loadStory(loadfile)
    # add (incremental?) undo buffer with input size given by user (w/ default value)
  
  def createBlankStory(self):
    self.h.printinfo("Creating new blank story")
    self.choices = {}
    self.contents = {}
    self.cgraph = defaultdict(list)
    self.revcgraph = defaultdict(list)
    self.startChoice = ""
  
  def loadStory(self,loadfile):
    self.h.printinfo("Attempting to load story from %s"%loadfile)
    
    try:
      f=open(loadfile,"r")
    except:
      print("Load unsuccessful. Please check if the load file exists.")
      exit()
    
    try:
      loadObj = json.load(f)
    except:
      print("Load unsuccessful. Please check if the load file follows the JSON file format.")
      f.close()
      exit()
    f.close()
    
    try:
      self.choices = loadObj["choices"]
      self.contents = loadObj["contents"]
      self.cgraph = loadObj["cgraph"]
      self.revcgraph = loadObj["revcgraph"]
      self.startChoice = loadObj["startChoice"]
    except:
      print("Load unsuccessful. The file does not follow the Storytree format. \
      Please check the wiki page on this (PUT LINK HERE) to resolve the issue.")
      exit()
      
    self.h.printinfo("Load complete")
  
  def saveStory(self,savefile):
    self.h.printinfo("Saving current progress to %s"%savefile)
    f=open(savefile,"w")
    f.write(self.getStoryJSON())
    f.close()
    # add incremental saves
    self.h.printinfo("Save complete")
  
  def getStoryJSON(self):
    return json.dumps({"choices":self.choices, 
    "contents":self.contents, 
    "cgraph":self.cgraph, 
    "revcgraph":self.revcgraph, 
    "startChoice":self.startChoice},
    indent=4)
  
  def addChoice(self,cid,choice,content):
    # add autocounter for ID
    wr = True
    self.h.printinfo("Adding choice %s"%cid)
    if cid in self.choices.keys():
      opt_selected = self.h.inputOptLoop([['O','o'],['S','s']], 
      topmsg="WARNING: ID \'%s\' already exists. Overwrite(Oo) or skip(Ss)?"%cid, 
      chosenmsg=["Overwriting choice %s"%cid, "Skipping choice %s"%cid])
      if opt_selected=='S':
        wr=False
    if wr:
      self.choices[cid] = choice
      self.contents[cid] = content
  
  def connectChoices(self,parentID,childID):
    self.h.catchAssert(parentID in self.choices.keys(), "Source choice ID %s does not exist. Exiting"%parentID)
    self.h.catchAssert(childID in self.choices.keys(), "Destination choice ID %s does not exist. Exiting"%childID)
    self.h.printinfo("Connecting choices \'%s\' and \'%s\'"%(parentID,childID))
    self.cgraph[parentID].append(childID)
    self.revcgraph[childID].append(parentID)
  
  def setStartChoice(self,cid):
    self.h.catchAssert(cid in self.choices.keys(), "Choice ID %s does not exist. Exiting"%cid)
    self.h.printinfo("Setting choice ID \'%s\'as start choice"%cid)
    self.startChoice = cid
  
  def getChoice(self,cid):
    self.h.catchAssert(cid in self.choices.keys(), "Choice ID %s does not exist. Exiting"%cid)
    self.h.printinfo("Printing choice %s"%cid)
    print("--- Choice Details Begin ---\nChoice ID: \'%s\'\nChoice text:\n\t%s\nChoice contents:\n\t%s\n--- Choice Details Finish ---"%(cid,self.choices[cid],self.contents[cid]))
  
  def choiceDFS(cnid,**kwargs):
    for nextid in cgraph[cnid]:
      choiceDFS(nextid)
  
  def deleteChoice(self,cid,delSubtree=True):
    # assert ID existence and give warnings
    # cycle errors
    if delSubtree:
      for nextid in self.cgraph[cid]:
        self.deleteChoice(nextid,True)
    else:
      for child in self.cgraph[cid]:
        self.revcgraph[child].remove(cid)
    
    for parent in self.revcgraph[cid]:
      self.cgraph[parent].remove(cid)
    self.choices.pop(cid)
    self.contents.pop(cid)
    self.cgraph.pop(cid)
    self.revcgraph.pop(cid)
    if self.startChoice==cid:
      choiceopts = [[_] for _ in self.choices.keys()]
      choiceopts.append([""])
      newStart = self.h.inputOptLoop(choiceopts,topmsg="CAUTION: Start choice \'%s\' has been deleted. Please provide a new start choice"%cid,invalidmsg="This start choice does not exist. Please provide a valid one")
      if newStart=="":
        self.h.printinfo("CAUTION: Start choice has been left empty")
      else:
        self.h.printinfo("Setting new start choice as \'%s\'"%newStart)
      self.startChoice = newStart
    
    

class story(storyV1):
  pass

'''
check out:
  pydot
  dotty-dict
  networkx
  tikzplotlib
'''
