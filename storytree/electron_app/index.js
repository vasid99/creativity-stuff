const {app, BrowserWindow} = require("electron")

function harness(){
  const mainWindow = new BrowserWindow({
    width: 1024,
    height: 576
  })
  
  mainWindow.loadFile("html/mainWindow.html")
}

app.on("ready",()=>harness())

