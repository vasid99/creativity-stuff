import scenedetect as sd
from sys import argv

# video name check
try:
	argv[1]
except:
	print("Usage: python %s video_file [srt_filename]"%(argv[0]))
	exit()

# set up video manager
try:
	vm = sd.VideoManager([argv[1]])
except OSError:
	print("ERROR: VideoManager OS error, check video file")
	exit()
base_timecode = vm.get_base_timecode()
duration = vm.get_duration()[0]
fps = vm.get_framerate()

# set up scene manager
sm = sd.SceneManager()
sm.add_detector(sd.detectors.content_detector.ContentDetector())

# do the detection
vm.set_downscale_factor()
vm.start()

sm.detect_scenes(frame_source=vm)
scenelist = sm.get_scene_list(base_timecode)

vm.release()

# set up output sub file
try:
	srtname = argv[2]
except:
	srtname = ""
	videoname_split = argv[1].split('.')
	for i in range(len(videoname_split)-1):
		srtname += videoname_split[i]+'.'
	srtname += "srt"

# write to output sub file
srtout = open(srtname,"w")
for i in range(len(scenelist)):
	srtout.write("%d\n%s --> %s\nclip-%d\n\n"%(i+1,scenelist[i][0],scenelist[i][1],i+1))
srtout.close()

"""
NOTES:
FrameTimecode: to store accurate frame numbers (basically (frame_number,framerate))
VideoManager:  to interface with video files
SceneManager:  to co-ordinate scene detectors with video sources
StatsManager:  to allow SceneManager to read/write metrics
SceneDetector: duh
"""
