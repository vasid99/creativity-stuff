# MV/Supercut Utilities
Some useful tools for (A)MV/supercut creators (MV = music video)

## Tools in this repo
1. vid2srt.py - detect scenes using jumpcuts, number the scenes and export them as a subtitle (SRT) file
Run `python vid2srt.py` to see usage.

## Other tools I find useful
### Video Container Tools
1. MP4Tool (Windows)
2. mkvtoolnix (all)

### Video Converters/Transcoders
1. Handbrake (all)
